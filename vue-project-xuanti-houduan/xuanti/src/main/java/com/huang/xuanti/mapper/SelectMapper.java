package com.huang.xuanti.mapper;

import com.huang.xuanti.entity.Select;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huang
 * @since 2022-02-10
 */
public interface SelectMapper extends BaseMapper<Select> {

}
