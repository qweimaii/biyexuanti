package com.huang.xuanti.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author huang
 * @since 2022-02-10
 */
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class User implements Serializable {

    // 使后端传给前端的id不会出现精度丢失问题
    //@JsonFormat 用来表示json序列化的一种格式或者类型，shap表示序列化后的一种类型
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(type = IdType.ID_WORKER)
    private Long u_id;

    private String num;

    private String name;

    private String password;

    private String mail;

    private String phone;

    private String major;

    private Integer u_group;

    private String school;

    private Integer type;

    public Long getU_id() {
        return u_id;
    }

    public void setU_id(Long u_id) {
        this.u_id = u_id;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Integer getU_group() {
        return u_group;
    }

    public void setU_group(Integer u_group) {
        this.u_group = u_group;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "User [mail=" + mail + ", major=" + major + ", name=" + name + ", num=" + num + ", password=" + password
                + ", phone=" + phone + ", school=" + school + ", type=" + type + ", u_group=" + u_group + ", u_id="
                + u_id + "]";
    }


    
    

    
}
