package com.huang.xuanti.service.impl;

import com.huang.xuanti.entity.User;
import com.huang.xuanti.mapper.UserMapper;
import com.huang.xuanti.service.IUserService;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huang
 * @since 2022-02-10
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    public List<User> studentStateByTid(Long id){
        return this.baseMapper.studentStateByTid(id);
    }

    public String subjectTitleBySid(Long sId){
        return this.baseMapper.subjectTitleBySid(sId);
    };
}
