package com.huang.xuanti.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.huang.xuanti.entity.Select;
import com.huang.xuanti.result.Result;
import com.huang.xuanti.service.ISelectService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author huang
 * @since 2022-02-10
 */
@RestController
public class FileController {
    @Autowired
    ISelectService selectService;

    

    //上传文件
    @PostMapping("/upload/{u_id}/{type}")
    public Result uploadFile(MultipartFile file,@PathVariable("u_id") Long u_id,@PathVariable("type") String type) throws IllegalStateException, IOException{
        String rootPath=System.getProperty("user.dir")+"/xuanti/src/main/resources/files";
        String fileName=file.getOriginalFilename(); 
        String folder=rootPath+"/"+u_id+"/"+type;

        File filePath=new File(folder);
        //如果不存在文件夹就新创一个
        if (!filePath.exists()){
            filePath.mkdirs();
        }else{
            //如果有文件夹，先把旧文件删掉
            File[] array = filePath.listFiles();
            System.out.println(array.length);
            if(array.length!=0){
                File fileRemove=new File(folder,array[0].getName());
                fileRemove.delete(); 
            }
        }
        file.transferTo(new File(folder,fileName));

        UpdateWrapper<Select> updateWrapper=new UpdateWrapper<>();
        //传过来的类型就是要更新的字段名，值为文件名
        updateWrapper.set(type, fileName).eq("u_id", u_id);
        selectService.update(updateWrapper);
        return Result.success(null);  
    }


    //删除文件
    @PostMapping("/delete/{u_id}/{type}")
    public Result deleteFile(@PathVariable("u_id") Long u_id,@PathVariable("type") String type){
        String rootPath=System.getProperty("user.dir")+"/xuanti/src/main/resources/files";
        String folder=rootPath+"/"+u_id+"/"+type;
        File filePath=new File(folder);
        //删除文件夹下的文件
        File[] array = filePath.listFiles();
        File fileRemove=new File(folder,array[0].getName());
        fileRemove.delete();
        //数据库移除这个文件的文件名
        UpdateWrapper<Select> updateWrapper=new UpdateWrapper<>();
        updateWrapper.set(type, null).eq("u_id", u_id);
        selectService.update(updateWrapper);

        return Result.success(null);
    }

    //下载文件
    @RequestMapping("/download/{u_id}/{type}")
    public void downloadFile(@PathVariable("u_id") Long u_id,@PathVariable("type") String type,HttpServletResponse response)throws IOException{
        String rootPath=System.getProperty("user.dir")+"/xuanti/src/main/resources/files";
        String folder=rootPath+"/"+u_id+"/"+type;
        File filePath=new File(folder);
        File[] array = filePath.listFiles();
        String fileName=array[0].getName();

        File downloadFile = new File(folder,fileName);
        // 将文件写入输入流
        FileInputStream fileInputStream = new FileInputStream(downloadFile);
        InputStream inputStream = new BufferedInputStream(fileInputStream);
        byte[] buffer = new byte[inputStream.available()];
        inputStream.read(buffer);
        inputStream.close();

        // 清空response
        response.reset();
        // 设置response的Header
        response.setCharacterEncoding("UTF-8");
        //Content-Disposition的作用：告知浏览器以何种方式显示响应返回的文件，用浏览器打开还是以附件的形式下载到本地保存
        //attachment表示以附件方式下载 inline表示在线打开 "Content-Disposition: inline; filename=文件名.mp3"
        // filename表示文件的默认名称，因为网络传输只支持URL编码的相关支付，因此需要将文件名URL编码后进行传输,前端收到后需要反编码才能获取到真正的名称
        response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
        // 告知浏览器文件的大小
        response.addHeader("Content-Length", "" + downloadFile.length());
        OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
        //描述了数据类型,APPLICATION/OCTET- STREAM代表二进制数据
        response.setContentType("application/octet-stream");
        outputStream.write(buffer);
        outputStream.flush();
    }
}
