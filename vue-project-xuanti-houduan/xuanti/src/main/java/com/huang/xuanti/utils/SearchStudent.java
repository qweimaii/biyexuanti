package com.huang.xuanti.utils;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.huang.xuanti.entity.User;
import com.huang.xuanti.service.IUserService;

import org.springframework.beans.factory.annotation.Autowired;

public class SearchStudent {
    @Autowired
    IUserService userService;
    public User searchStudentByNameAndNum(String name,String num){
        QueryWrapper<User> wrapper = new QueryWrapper<>();
            wrapper.eq("num", num).eq("name", name);
            User user=userService.getOne(wrapper);
        return user;
    }
}
