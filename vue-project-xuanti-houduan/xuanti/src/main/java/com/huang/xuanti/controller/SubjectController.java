package com.huang.xuanti.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.huang.xuanti.entity.Select;
import com.huang.xuanti.entity.Subject;
import com.huang.xuanti.entity.SubjectList;
import com.huang.xuanti.entity.User;
import com.huang.xuanti.result.Result;
import com.huang.xuanti.service.ISelectService;
import com.huang.xuanti.service.ISubjectService;
import com.huang.xuanti.service.IUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author huang
 * @since 2022-02-10
 */
@RestController
public class SubjectController {
    @Autowired
    ISubjectService subjectService;
    @Autowired
    IUserService userService;
    @Autowired
    ISelectService selectService;

    //课题列表
    @GetMapping("/subjectlist/{currPage}/{recordPerPage}")
    public Result subjectList(@PathVariable("currPage") int currPage,
            @PathVariable("recordPerPage") int recordPerPage) {
        IPage<SubjectList> page = subjectService.subjectList(currPage, recordPerPage);
        return Result.success(page);
    }

    //根据搜索框和类型搜索课题
    @GetMapping("/searchsubject/{item}/{type}")
    public Result searchSubject(@PathVariable("type") String type, @PathVariable("item") String item) {
        List<Map<String, Object>> list = new ArrayList<>();
        if (type.equals("1")) {
            list = subjectService.searchSubject("user.name", '%' + item + '%');
        } else {
            list = subjectService.searchSubject("subject.title", '%' + item + '%');
        }
        return Result.success(list);
    }

    //课题详情
    // @GetMapping("/subjectDetailById/{id}")
    // public Result subjectDetailById(@PathVariable("id") Long id) {
    //     Map<String, Object> resultMap = new HashMap<>();
    //     Map<String, Object> map = subjectService.subjectDetailById(id);
    //     List<User> list = subjectService.studentListBySjId(id);
    //     resultMap.put("subjectDetail", map);
    //     resultMap.put("studentList", list);
    //     return Result.success(resultMap);
    // }

    @GetMapping("/subjectDetailById/{id}")
    public Result subjectDetailById(@PathVariable("id") Long id) {
        Map<String, Object> resultMap = new HashMap<>();
        //课题的详情
        QueryWrapper<Subject> subjectWrapper=new QueryWrapper<>();
        subjectWrapper.select("title","content","note","s_num","t_num","u_id").eq("sj_id", id);
        Subject subject=subjectService.getOne(subjectWrapper);
        //发布课题的老师的信息
        QueryWrapper<User> userWrapper=new QueryWrapper<>();
        userWrapper.select("name","mail","phone").eq("u_id", subject.getU_id());
        User user=userService.getOne(userWrapper);
        //选了该题的学生列表
        List<User> list = subjectService.studentListBySjId(id);
        resultMap.put("teacherDetail", user);
        resultMap.put("subjectDetail", subject);
        resultMap.put("studentList", list);
        return Result.success(resultMap);
    }


    //发布课题
    @PostMapping("/declaresubject/{teacherId}/{num}/{name}")
    public Result declareSubject(@PathVariable("teacherId") Long teacherId, @RequestBody Subject subject,
            @PathVariable("num") String num, @PathVariable("name") String name) {
        User user = new User();

        if (!num.equals("null") || !name.equals("null")) {
            QueryWrapper<User> wrapper = new QueryWrapper<>();
            wrapper.eq("num", num).eq("name", name);
            user = userService.getOne(wrapper);
        }

        if (user == null) {
            return Result.res(202, "申报课题失败，指定的学生不存在", null);
        } else {
            Date time = new java.sql.Date(new java.util.Date().getTime());
            Long id = IdWorker.getId();
            subject.setU_id(teacherId);
            subject.setCreate_time(time);
            subject.setSj_id(id);
            subjectService.save(subject);
            if (user.getU_id() != null) {
                Map<String, Object> map = new HashMap<>();
                map.put("u_id", user.getU_id()+"");
                map.put("sj_id", id+"");
                //申报课题成功，返回指定学生
                return Result.res(210, "", map);
            }else{
                return Result.success(null);
            }
        }
    }

    //编辑修改课题
    @PostMapping("/updatesubject/{id}")
    public Result updateSubject(@RequestBody Subject subject,@PathVariable("id") Long id){
        UpdateWrapper<Subject> updateWrapper=new UpdateWrapper<>();
        updateWrapper.set("title", subject.getTitle())
        .set("content", subject.getContent())
        .set("note", subject.getNote())
        .eq("sj_id", id);
        subjectService.update(updateWrapper);
        return Result.success(null);
    }

    //删除课题
    @PostMapping("/deletesubject/{id}")
    public Result deleteSubject(@PathVariable("id") Long id){
        QueryWrapper<Select> wrapper=new QueryWrapper<>();
        wrapper.eq("sj_id", id);
        //先把选了这个课题的选课记录删了
        selectService.remove(wrapper);
        subjectService.removeById(id);
        return Result.success(null);
    }


    
}
