package com.huang.xuanti.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.huang.xuanti.entity.Select;
import com.huang.xuanti.entity.Subject;
import com.huang.xuanti.result.Result;
import com.huang.xuanti.service.ISelectService;
import com.huang.xuanti.service.ISubjectService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author huang
 * @since 2022-02-10
 */
@RestController
public class SelectController {
    @Autowired
    ISelectService selectService;
    @Autowired
    ISubjectService subjectService;


    //选题 一个学生只能选一个
    @RequestMapping("selectsubject/{sj_id}/{u_id}")
    public Result selectSubject(@PathVariable("sj_id") Long sj_id, @PathVariable("u_id") Long u_id) {
        //在select表里找学生的选课记录，有记录说明已经选择了一个课题
        QueryWrapper<Select> selectWrapper=new QueryWrapper<>();
        selectWrapper.eq("u_id", u_id);
        if(selectService.getOne(selectWrapper) !=null){
            return Result.res(203, "已经选择了一个课题", null);
        } 

        // 找到要选择的课题，判断课题是否已选满
        QueryWrapper<Subject> subjectWrapper = new QueryWrapper<>();
        subjectWrapper.select("s_num", "t_num").eq("sj_id", sj_id);
        Subject subject = subjectService.getOne(subjectWrapper);
        int totalNumber = subject.getT_num();
        int selectedNumber = subject.getS_num();
        if (selectedNumber == totalNumber) {
            // 选满
            return Result.res(202, "该选题选择人数已满", null);
        } else {
            // 未选满 ，已选+1
            selectedNumber += 1;
            UpdateWrapper<Subject> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("s_num", selectedNumber).eq("sj_id", sj_id);
            subjectService.update(updateWrapper);
            // 添加选题记录
            Select select = new Select();
            select.setSj_id(sj_id);
            select.setU_id(u_id);
            selectService.save(select);
            return Result.success(null);
        }

    }

    //退选
    @RequestMapping("removeselect/{u_id}")
    public Result removeSelectSubject(@PathVariable("u_id") Long u_id) {
        //把该用户选题记录的选题id拿出来
        QueryWrapper<Select> selectWrapper=new QueryWrapper<>();
        selectWrapper.select("sj_id").eq("u_id", u_id);
        Select select=selectService.getOne(selectWrapper);
        Long sj_id=select.getSj_id();
        //清除该选题记录
        selectService.remove(selectWrapper);
        //对应的选题的人数减一
        QueryWrapper<Subject> subjectWrapper=new QueryWrapper<>();
        subjectWrapper.select("s_num").eq("sj_id", sj_id);
        Subject subject= subjectService.getOne(subjectWrapper);
        int selectedNumber=subject.getS_num();
        selectedNumber -= 1;
        UpdateWrapper<Subject> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("s_num", selectedNumber).eq("sj_id", sj_id);
        subjectService.update(updateWrapper);
        return Result.success(null);
    }


    //编写意见
    @PostMapping("/updateadvice/{se_id}/{advice}")
    public Result updateAdvice(@PathVariable("se_id") Long se_id,@PathVariable("advice") String advice) {
        UpdateWrapper<Select> updateWrapper =new UpdateWrapper<>();
        updateWrapper.set("advice", advice).eq("se_id", se_id);
        selectService.update(updateWrapper);
        return Result.success(null);
    }

}
