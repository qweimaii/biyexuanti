package com.huang.xuanti.mapper;

import com.huang.xuanti.entity.Subject;
import com.huang.xuanti.entity.SubjectList;
import com.huang.xuanti.entity.User;

import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huang
 * @since 2022-02-10
 */
public interface SubjectMapper extends BaseMapper<Subject> {
    @Select("SELECT subject.sj_id,subject.u_id,subject.title,subject.s_num,subject.t_num,subject.create_time,user.name FROM subject LEFT JOIN user ON subject.u_id=user.u_id")
    List<SubjectList> subjectList(Page<SubjectList> page);

    @Select("SELECT subject.sj_id,subject.u_id,subject.title,subject.s_num,subject.t_num,subject.create_time,user.name FROM subject LEFT JOIN user ON subject.u_id=user.u_id WHERE ${column} like #{name}")
    List<Map<String,Object>> searchSubject(String column, String name);

    //弃用
    @Select("SELECT subject.title,subject.content,subject.note,subject.s_num,subject.t_num,user.name,user.mail,user.phone FROM subject LEFT JOIN user ON subject.u_id=user.u_id WHERE sj_id=#{id}")
    Map<String,Object> subjectDetailById(Long id);

    @Select("SELECT user.`name`,user.num,user.u_id FROM user,`select` WHERE `select`.u_id=user.u_id AND `select`.sj_id=#{id}")
    List<User> studentListBySjId(Long id);
}
