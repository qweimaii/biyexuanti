package com.huang.xuanti.service;

import com.huang.xuanti.entity.Select;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huang
 * @since 2022-02-10
 */
public interface ISelectService extends IService<Select> {

}
