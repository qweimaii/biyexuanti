package com.huang.xuanti.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author huang
 * @since 2022-02-10
 */
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("`select`")
public class Select implements Serializable {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(type = IdType.ID_WORKER)
    private Long se_id;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long sj_id;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long u_id;

    private String mission;

    private String lite_review;

    private String let_en;

    private String let_cn;

    private String pap_check;

    private String pap_fis;

    private String pap_mid;

    private String pap_fin;

    private String advice;

    public Long getSe_id() {
        return se_id;
    }

    public void setSe_id(Long se_id) {
        this.se_id = se_id;
    }

    public Long getSj_id() {
        return sj_id;
    }

    public void setSj_id(Long sj_id) {
        this.sj_id = sj_id;
    }

    

    public Long getU_id() {
        return u_id;
    }

    public void setU_id(Long u_id) {
        this.u_id = u_id;
    }

    public String getMission() {
        return mission;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public String getLite_review() {
        return lite_review;
    }

    public void setLite_review(String lite_review) {
        this.lite_review = lite_review;
    }

    public String getLet_en() {
        return let_en;
    }

    public void setLet_en(String let_en) {
        this.let_en = let_en;
    }

    public String getLet_cn() {
        return let_cn;
    }

    public void setLet_cn(String let_cn) {
        this.let_cn = let_cn;
    }

    public String getPap_check() {
        return pap_check;
    }

    public void setPap_check(String pap_check) {
        this.pap_check = pap_check;
    }

    public String getPap_fis() {
        return pap_fis;
    }

    public void setPap_fis(String pap_fis) {
        this.pap_fis = pap_fis;
    }

    public String getPap_mid() {
        return pap_mid;
    }

    public void setPap_mid(String pap_mid) {
        this.pap_mid = pap_mid;
    }

    public String getPap_fin() {
        return pap_fin;
    }

    public void setPap_fin(String pap_fin) {
        this.pap_fin = pap_fin;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    @Override
    public String toString() {
        return "Select [advice=" + advice + ", let_cn=" + let_cn + ", let_en=" + let_en + ", lite_review=" + lite_review
                + ", mission=" + mission + ", pap_check=" + pap_check + ", pap_fin=" + pap_fin + ", pap_fis=" + pap_fis
                + ", pap_mid=" + pap_mid + ", se_id=" + se_id + ", sj_id=" + sj_id + ", u_id=" + u_id + "]";
    }

    

    

    
    
}
