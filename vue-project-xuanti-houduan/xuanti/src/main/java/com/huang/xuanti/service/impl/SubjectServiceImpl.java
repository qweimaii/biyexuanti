package com.huang.xuanti.service.impl;

import com.huang.xuanti.entity.Subject;
import com.huang.xuanti.entity.SubjectList;
import com.huang.xuanti.entity.User;
import com.huang.xuanti.mapper.SubjectMapper;
import com.huang.xuanti.service.ISubjectService;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huang
 * @since 2022-02-10
 */
@Service
public class SubjectServiceImpl extends ServiceImpl<SubjectMapper, Subject> implements ISubjectService {
    @Override
    public Page<SubjectList> subjectList(int currPage,int recordPerPage){
        Page<SubjectList> page=new Page<SubjectList>(currPage,recordPerPage);
        return page.setRecords(this.baseMapper.subjectList(page));
        
    }

    @Override
    public List<Map<String,Object>> searchSubject(String column,String name){
        return this.baseMapper.searchSubject(column,name);
    }

    @Override
    public Map<String,Object> subjectDetailById(Long id){
        return this.baseMapper.subjectDetailById(id);
    }

    @Override
    public List<User> studentListBySjId(Long id){
        return this.baseMapper.studentListBySjId(id);
    }
}
