package com.huang.xuanti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XuantiApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(XuantiApplication.class, args);
	}

}
