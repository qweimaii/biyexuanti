package com.huang.xuanti.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.sql.Date;

import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author huang
 * @since 2022-02-10
 */
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Subject implements Serializable {

    // @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(type = IdType.INPUT)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long sj_id;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long u_id;

    private String title;

    private String content;

    private String note;

    private Date create_time;

    private Integer s_num;

    private Integer t_num;

    public Long getSj_id() {
        return sj_id;
    }

    public void setSj_id(Long sj_id) {
        this.sj_id = sj_id;
    }

    public Long getU_id() {
        return u_id;
    }

    public void setU_id(Long u_id) {
        this.u_id = u_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Integer getS_num() {
        return s_num;
    }

    public void setS_num(Integer s_num) {
        this.s_num = s_num;
    }

    public Integer getT_num() {
        return t_num;
    }

    public void setT_num(Integer t_num) {
        this.t_num = t_num;
    }

    @Override
    public String toString() {
        return "Subject [content=" + content + ", create_time=" + create_time + ", note=" + note + ", s_num=" + s_num
                + ", sj_id=" + sj_id + ", t_num=" + t_num + ", title=" + title + ", u_id=" + u_id + "]";
    }

    

    
}
