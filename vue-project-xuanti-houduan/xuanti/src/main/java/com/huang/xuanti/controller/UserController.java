package com.huang.xuanti.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huang.xuanti.entity.Select;
import com.huang.xuanti.entity.User;
import com.huang.xuanti.result.Result;
import com.huang.xuanti.service.ISelectService;
import com.huang.xuanti.service.ISubjectService;
import com.huang.xuanti.service.IUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author huang
 * @since 2022-02-10
 */
@RestController

public class UserController {
    @Autowired
    IUserService userService;
    @Autowired
    ISubjectService subjectService;
    @Autowired
    ISelectService selectService;

    //学生列表
    @GetMapping("/studentlist/{currPage}/{recordPerPage}")
    public Result queStudentList(@PathVariable(name = "currPage") int currPage,
            @PathVariable(name = "recordPerPage") int recordPerPage) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.select("u_id", "num", "name", "mail", "phone", "major", "school").eq("type", 0);
        Page<User> page = new Page<>(currPage, recordPerPage);
        IPage<User> userPage = userService.page(page, wrapper);
        return Result.success(userPage);
    }

    //添加学生
    @PostMapping("/addstudent")
    public Result addStudent(@RequestBody User user) {
        String num = user.getNum();
        String password = num.substring(num.length() - 4);
        user.setPassword("pass" + password);
        userService.save(user);
        return Result.success(null);
    }

    //修改学生 如果传过来的user某一项为null且出现在下面的更新项中会提交null覆盖，数据库中num不能为空
    @PostMapping("/updatestudent")
    public Result updateStudent(@RequestBody User user) {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("num", user.getNum())
                .set("name", user.getName())
                .set("school", user.getSchool())
                .set("major", user.getMajor())
                .set("phone", user.getPhone())
                .set("mail", user.getMail()).eq("u_id", user.getU_id());
        userService.update(updateWrapper);
        return Result.success(null);
    }

    //删除学生
    @PostMapping("/deletestudent/{id}")
    public Result deleteStudent(@PathVariable("id") Long id) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("u_id", id);
        userService.remove(wrapper);
        return Result.success(null);
    }

    //根据搜索框和类型查找学生
    @GetMapping("/searchstudent/{item}/{type}")
    public Result searchStudent(@PathVariable("type") String type, @PathVariable("item") String item) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.select("u_id", "num", "name", "mail", "phone", "major", "school");
        if (type.equals("1")) {
            wrapper.like("num", item);
        } else {
            wrapper.like("name", item);
        }
        List<User> list = userService.list(wrapper);
        return Result.success(list);
    }

    //登录
    @PostMapping("/login")
    public Result userLogin(@RequestBody User user) {
        Map<String, Object> userMap = new HashMap<>();
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("num", user.getNum()).eq("password", user.getPassword());
        User result = userService.getOne(wrapper);

        if (result == null) {
            return Result.res(202, "学号或密码错误", null);
        }
        userMap.put("u_id", result.getU_id() + "");
        userMap.put("num", result.getNum());
        userMap.put("name", result.getName());
        userMap.put("type", result.getType());
        userMap.put("major", result.getMajor());
        return Result.success(userMap);
    }

    //学生状态列表（选了用户发表的课题的学生）
    @GetMapping("/studentstate/{teacher_id}")
    public Result studentState(@PathVariable("teacher_id") Long teacher_id) {
        List<User> list = userService.studentStateByTid(teacher_id);
        return Result.success(list);
    }

    //分组
    @PostMapping("/updategroup/{id}/{group}")
    public Result updateGroup(@PathVariable("id") Long id,@PathVariable("group")int group){
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("u_group", group)
                .eq("u_id", id);
        userService.update(updateWrapper);
        return Result.success(null);
    }

    //学生状态详情
    @GetMapping("/statedetail/{id}")
    public Result stateDetail(@PathVariable("id") Long id){
        QueryWrapper<User> userWrapper=new QueryWrapper<>();
        userWrapper.select( "num", "name", "mail", "phone", "major", "school").eq("u_id", id);
        User user=userService.getOne(userWrapper);

        QueryWrapper<Select> selectWrapper=new QueryWrapper<>();
        selectWrapper.select( "se_id", "mission","lite_review","let_en", "let_cn", "pap_check", "pap_fis","pap_mid","pap_fin","advice").eq("u_id", id);
        Select select=selectService.getOne(selectWrapper);
        //根据学生id查找选题题目
        String subjectTitle=userService.subjectTitleBySid(id);
        Map<String,Object> map=new HashMap<>();
        map.put("selectData", select);
        map.put("userData", user);
        map.put("subjectTitle", subjectTitle);
        return Result.success(map);
    }

    //个人详情
    @GetMapping("/personinfo/{u_id}")
    public Result personInfo(@PathVariable("u_id") Long u_id) {
        QueryWrapper<User> wrapper=new QueryWrapper<>();
        wrapper.select("name","num","major","school","phone","mail").eq("u_id", u_id);
        User user= userService.getOne(wrapper);
        return Result.success(user);
    }

    //完善个人信息
    @PostMapping("/updateinfo")
    public Result updateInfo(@RequestBody User user) {
        UpdateWrapper<User> updateWrapper=new UpdateWrapper<>();
        updateWrapper.set("mail", user.getMail());
        updateWrapper.set("phone", user.getPhone()).eq("u_id", user.getU_id());
        userService.update(updateWrapper);
        return Result.success(null);
    }
    
    //修改密码
    @PostMapping("/changepwd/{oldpwd}/{newpwd}/{u_id}")
    public Result changePassWord(@PathVariable("oldpwd") String oldpwd,
            @PathVariable("newpwd") String newpwd,
            @PathVariable("u_id") Long u_id){
        QueryWrapper<User> wrapper=new QueryWrapper<>();
        wrapper.select("password").eq("u_id", u_id);
        User user=userService.getOne(wrapper);
        if(!user.getPassword().equals(oldpwd)){
            return Result.res(202, "修改失败，原密码不正确", null);
        }else{
            UpdateWrapper<User> updateWrapper=new UpdateWrapper<>();
            updateWrapper.set("password", newpwd).eq("u_id", u_id);
            userService.update(updateWrapper);
            return Result.success(null);
        }
    }

    //学生选题的文件提交状态
    @GetMapping("/subjectfilestate/{u_id}")
    public Result subjectFileState(@PathVariable("u_id") Long u_id) {
        QueryWrapper<Select> wrapper=new QueryWrapper<>();
        wrapper.eq("u_id", u_id);
        Select select=selectService.getOne(wrapper);
        //根据学生id查找选题题目
        String subjectTitle=userService.subjectTitleBySid(u_id);
        
        Map<String,Object> map=new HashMap<>();
        map.put("selectData", select);
        map.put("subjectTitle", subjectTitle);
        return Result.success(map);
        
    }



}   
