package com.huang.xuanti.service.impl;

import com.huang.xuanti.entity.Select;
import com.huang.xuanti.mapper.SelectMapper;
import com.huang.xuanti.service.ISelectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huang
 * @since 2022-02-10
 */
@Service
public class SelectServiceImpl extends ServiceImpl<SelectMapper, Select> implements ISelectService {

}
