package com.huang.xuanti.mapper;

import com.huang.xuanti.entity.User;

import org.apache.ibatis.annotations.Select;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huang
 * @since 2022-02-10
 */
public interface UserMapper extends BaseMapper<User> {
    @Select("SELECT `user`.u_id,`user`.`name`,`user`.num,`user`.u_group FROM `user` WHERE `user`.u_id IN(SELECT `select`.u_id FROM `select` WHERE `select`.sj_id IN (SELECT `subject`.sj_id FROM `subject` WHERE `subject`.u_id=#{id}))ORDER BY `user`.u_group")
    List<User> studentStateByTid(Long id);

    @Select("SELECT `subject`.title FROM `subject`,`select` WHERE `select`.u_id=#{sId} AND `select`.sj_id=`subject`.sj_id")
    String subjectTitleBySid(Long sId);
}
