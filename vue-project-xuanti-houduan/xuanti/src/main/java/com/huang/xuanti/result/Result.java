package com.huang.xuanti.result;

import java.io.Serializable;

public class Result implements Serializable{
    private int code;
    private String msg;
    private Object data;
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
    public Object getData() {
        return data;
    }
    public void setData(Object data) {
        this.data = data;
    }

    public static Result success(Object data){
        return res(200, "成功", data);
    }

    public static Result res(int code, String msg, Object data) {
        Result re=new Result();
        re.setCode(code);
        re.setMsg(msg);
        re.setData(data);
        return re;
    }

    @Override
    public String toString() {
        return "result [code=" + code + ", data=" + data + ", msg=" + msg + "]";
    }
    
}
