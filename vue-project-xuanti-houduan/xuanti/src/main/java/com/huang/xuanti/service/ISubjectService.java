package com.huang.xuanti.service;

import com.huang.xuanti.entity.Subject;
import com.huang.xuanti.entity.SubjectList;
import com.huang.xuanti.entity.User;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huang
 * @since 2022-02-10
 */
public interface ISubjectService extends IService<Subject> {
    Page<SubjectList> subjectList(int currPage,int recordPerPage);
    List<Map<String,Object>> searchSubject(String column,String name);
    Map<String,Object> subjectDetailById(Long id);
    List<User> studentListBySjId(Long id);
}
