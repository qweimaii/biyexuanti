package com.huang.xuanti.service;

import com.huang.xuanti.entity.User;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huang
 * @since 2022-02-10
 */
public interface IUserService extends IService<User> {
    List<User> studentStateByTid(Long id);
    String subjectTitleBySid(Long sId);
}
