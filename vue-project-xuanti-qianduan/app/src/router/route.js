import Vue from "vue";
import VueRouter from "vue-router";
import routes from './routes.js'

//使用插件
Vue.use(VueRouter);
let router = new VueRouter({
    //配置路由
    routes: routes,
    //滚动行为，跳转路由后所在的位置
    scrollBehavior(to, from, savedPosition) {
        return { y: 0 }
    }

})
//全局守卫，如果未登录去其他页面（除了登录，欢迎路由）会转去登录页面，已登录则放行
router.beforeEach((to, from, next) => {
    if (to.meta.allowed) {
        if (localStorage.getItem("userInfo") == null) {
            next('/login')
        }else{
            next()
        }
    } else {
        next()
    }

})

export default router