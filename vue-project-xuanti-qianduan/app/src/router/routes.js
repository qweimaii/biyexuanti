import StudentStateList from '../pages/StudentStateList.vue'
import DeclareSubject from '../pages/DeclareSubject.vue'
import PersonInfo from '../pages/PersonInfo.vue'
import StudentList from '../pages/StudentList.vue'
import SubjectList from '../pages/SubjectList.vue'
import Welcome from '../pages/Welcome.vue'
import SubjectDetail from '../pages/SubjectDetail'
import MySubject from '../pages/MySubject'
import Login from '../pages/Login'
import {getType} from '../utils/getUserInfo'

export default [
    {
        name: 'Login',
        path: '/login',
        component: Login,
        //独享路由守卫
        beforeEnter: (to, from, next) => {
            //未登录放行，已登录则拦截(type==1,==0)
            let type=getType()
            if (type == 3) {
                next()
            }
        }
    },
    {
        name: 'MySubject',
        path: '/mysubject',
        component: MySubject,
        meta: { allowed: true },
        beforeEnter: (to, from, next) => {
            //学生放行
            let type=getType()
            if (type == 0) {
                next()
            }
        }
    },
    {
        name: 'SubjectDetail',
        path: '/subjectdetail',
        component: SubjectDetail,
        meta: { allowed: true }
    },
    {
        name: 'StudentStateList',
        path: '/studentstatelist',
        component: StudentStateList,
        meta: { allowed: true },
        beforeEnter: (to, from, next) => {
            //教师放行
            let type=getType()
            if (type == 1) {
                next()
            }
        }
    },
    {
        name: 'DeclareSubject',
        path: '/declaresubject',
        component: DeclareSubject,
        meta: { allowed: true },
        beforeEnter: (to, from, next) => {
            //教师放行
            let type=getType()
            if (type == 1) {
                next()
            }
        }
    },
    {
        name: 'PersonInfo',
        path: '/personinfo',
        component: PersonInfo,
        meta: { allowed: true }
    },
    {
        name: 'StudentList',
        path: '/studentlist',
        component: StudentList,
        meta: { allowed: true },
        beforeEnter: (to, from, next) => {
            //教师放行
            let type=getType()
            if (type == 1) {
                next()
            }
        }
    },
    {
        name: 'SubjectList',
        path: '/subjectlist',
        component: SubjectList,
        meta: { allowed: true }
    },
    {
        name: 'Welcome',
        path: '*',
        component: Welcome

    },
]





