import requests from './request';

//登录
export const postLogin=(user)=>{
    return requests({url:"/login",data:user,method:"post"})
}

//查询学生列表
export const reqStudentList=(currPage,recordPerPage)=>{
    return requests({url:`/studentlist/${currPage}/${recordPerPage}`,method:"get"})
}

//增加学生
export const postAddStudent=(student)=>{
    return requests({url:'/addstudent',data:student,method:"post"})
}

//修改学生
export const postUpdateStudent=(student)=>{
    return requests({url:"/updatestudent",data:student,method:"post"})
}

//删除学生
export const postDeleteStudent=(id)=>{
    return requests({url:`/deletestudent/${id}`,method:"post"})
}

//查询学生
export const reqSearchStudent=(item,type)=>{
    return requests({url:`/searchstudent/${item}/${type}`,method:"get"})
}

//获取课题列表
export const reqSubjectList=(currPage,recordPerPage)=>{
    return requests({url:`/subjectlist/${currPage}/${recordPerPage}`,method:"get"})
}

//搜索课题
export const reqSearchSubject=(item,type)=>{
    return requests({url:`/searchsubject/${item}/${type}`,method:"get"})
}

//课题详情
export const reqSubjectDetail=(sujectId)=>{
    return requests({url:`/subjectDetailById/${sujectId}`,method:"get"})
}

//申报课题
export const postDeclareSubject=(teacherId,num,name,subject)=>{
    return requests({url:`/declaresubject/${teacherId}/${num}/${name}`,data:subject,method:"post"})
}

//修改课题
export const postUpdateSubject=(id,subject)=>{
    return requests({url:`/updatesubject/${id}`,data:subject,method:"post"})
}

//删除课题
export const postDeleteSubject=(id)=>{
    return requests({url:`/deletesubject/${id}`,method:"post"})
}

//学生状态列表
export const reqStudentState=(teacherId)=>{
    return requests({url:`/studentstate/${teacherId}`,method:"get"})
}

//学生状态详情
export const reqStateDetail=(studentId)=>{
    return requests({url:`/statedetail/${studentId}`,method:"get"})
}

//分组
export const postUpdateGroup=(id,group)=>{
    return requests({url:`/updategroup/${id}/${group}`,method:"post"})
}

//编写意见
export const postUpdateAdvice=(se_id,advice)=>{
    return requests({url:`/updateadvice/${se_id}/${advice}`,method:"post"})
}

//个人信息
export const getPersonInfo=(u_id)=>{
    return requests({url:`/personinfo/${u_id}`,method:"get"})
}

//完善个人信息
export const postUpdateInfo=(user)=>{
    return requests({url:'/updateinfo',data:user,method:"post"})
}

//修改密码
export const postChangePwd=(oldpwd,newpwd,u_id)=>{
    return requests({url:`/changepwd/${oldpwd}/${newpwd}/${u_id}`,method:"post"})
}

//上传文件
export const postUploadFile=(u_id,type,file)=>{
    return requests({url:`/upload/${u_id}/${type}`,data:file,method:"post"})
}

//删除文件
export const postRemoveFile=(u_id,type)=>{
    return requests({url:`/delete/${u_id}/${type}`,method:"post"})
}


// 课题文件详情
export const getFileState=(u_id)=>{
    return requests({url:`/subjectfilestate/${u_id}`,method:"get"})
}

//选题
export const postSelectSubject=(sj_id,u_id)=>{
    return requests({url:`selectsubject/${sj_id}/${u_id}`,method:"post"})
}

//退选
export const postRemoveSelect=(u_id)=>{
    return requests({url:`removeselect/${u_id}`,method:"post"})
}

