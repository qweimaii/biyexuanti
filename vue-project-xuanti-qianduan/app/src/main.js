import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import route from './router/route'

Vue.use(ElementUI);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router:route
}).$mount('#app')

// 防止localStorage被修改
window.addEventListener('storage', function(e) {
  localStorage.setItem(e.key, e.oldValue)
});