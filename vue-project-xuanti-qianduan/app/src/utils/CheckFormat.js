export const checkPhone=(phone)=>{
    //search() 方法 用于检索字符串中指定的子字符串，或检索与正则表达式相匹配的子字符串，并返回子串的起始位置。
    const phonePattern = /^1[3-9]\d{9}$/;
    if(phone.length==0){
        return true
    }else if(phone.search(phonePattern) != -1){
        return true
    }else{
        return false
    }
}

export const checkMail=(mail)=>{
    const emailPattern = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(mail.length==0){
        return true
    }else if(mail.search(emailPattern) != -1){
        return true
    }else{
        return false
    }
}

