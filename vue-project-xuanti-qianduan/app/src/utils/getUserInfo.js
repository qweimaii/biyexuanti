export const getType=()=>{
    let type=3
    if (localStorage.getItem("userInfo") != null) {
        type = JSON.parse(localStorage.getItem("userInfo")).type
    }
    return type
}

export const getUid=()=>{
    return JSON.parse(localStorage.getItem("userInfo")).u_id
}